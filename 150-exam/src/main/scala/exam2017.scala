// Your name and ITU email: ____
package adpro.exam2017

import scala.language.higherKinds
import fpinscala.monoids.Monoid
import fpinscala.monads.Functor
import fpinscala.state.State
import fpinscala.laziness.{Stream,Empty,Cons}
import fpinscala.laziness.Stream._
import fpinscala.parallelism.Par._
import adpro.data._
import adpro.data.FingerTree._
import monocle.Lens

object Q1 { 

  def hasKey[K,V] (l: List[(K,V)]) (k: K) :Boolean = 
    l exists {case (ke,_) => ke == k}

  def reduceByKey[K,V] (l :List[(K,V)]) (ope: (V,V) => V) :List[(K,V)] = 
  {
    def putInto[K,V] (kv: (K,V)) (acc: List[(K,V)]) (ope: (V,V) => V) : List[(K,V)] =
    {
      if (hasKey (acc) (kv._1)) 
          acc map {case (k,v) => if (k == kv._1) (k,ope(v,kv._2)) else (k,v)}
      else kv::acc
    }

    l.foldLeft[List[(K,V)]] (Nil) ((acc, kv) => putInto (kv) (acc) (ope))
  }

  def separate (l :List[(Int,List[String])]) :List[(Int,String)] =
    l flatMap { idws => idws._2 map { w => (idws._1,w) } }

  def separateViaFor (l :List[(Int,List[String])]) :List[(Int,String)] = 
    for {
      idws <- l
      w <- idws._2
    } yield (idws._1,w)


} // Q1


object Q2 {

  trait TreeOfLists[+A]
  case object LeafOfLists  extends TreeOfLists[Nothing]
  case class BranchOfLists[+A] (
    data: List[A],
    left: TreeOfLists[A],
    right: TreeOfLists[A]
  ) extends TreeOfLists[A]

  trait TreeOfCollections[C[+_],+A]
  case object LeafOfCollections  extends TreeOfCollections[C,Nothing]
  case class BranchOfCollections[C[+_],+A] (
    data: C[A],
    left: TreeOfCollections[C,A],
    right: TreeOfCollections[C,A]
  ) extends TreeOfCollections[C,A]
  // trait TreeOfCollections[...]
  // case class LeafOfCollections ...
  // case class BranchOfCollections ...

  def map[A,B] (t: TreeOfLists[A]) (f: A => B) :TreeOfLists[B] = t match {
    case LeafOfLists => LeafOfLists
    case BranchOfLists (data,left,right) =>
        BranchOfLists (data map f, map (left) (f), map (right) (f))
  }

  // def map[...] (t: TreeOfCollections[...]) (f: A => B) ...

} // Q2

object Q3 {

  def p (n: Int): Int = { println (n.toString); n }

  def f (a: Int, b: Int): Int = if (a > 10) a else b

  // Answer the questions in comments here

  // A. 
  //p(42) prints 42 as a string and returns 42
  //p(7) prints 7 as a string and returns 42
  //f (p42,p7) returns 42
  //p(f (p42,p7)) prints 42 as a string and returns 42
  // soo:
  //42
  //7
  //42

  // B.
  //42
  //42
  //42

  // C.
  //42
  //42

} // Q3


object Q4 {

  sealed trait Input
  case object Coin extends Input
  case object Brew extends Input

  case class MachineState (ready: Boolean, coffee: Int, coins: Int)

  def step (i: Input) (s: MachineState) :MachineState = 
    (i,s) match {
      case(_, MachineState(_,0,_)) => s
      case(Brew, MachineState(true,_,_)) => s
      case(Brew, MachineState(false,brew,_)) => MachineState(true, brew - 1, _)
      case(Coin, MachineState(false, _, coin)) => MachineState(false, _, coin + 1)
      case(Coin, MachineState(true, _, coin)) => MachineState(false, _, coin + 1)
      

    }

  def simulateMachine (initial: MachineState) (inputs: List[Input]) :(Int,Int) = 
    for {
      _ <- 
    }

} // Q4


object Q5 {

  def flatten[A] (s: =>Stream[List[A]]) :Stream[A] = ???

} // Q5


object Q6 {

  def parExists[A] (as: List[A]) (p: A => Boolean): Par[Boolean] = ???

} // Q6


object Q7 {

  //  def reduceL[A,Z] (opl: (Z,A) => Z) (z: Z, t: FingerTree[A]) :Z = ??? // assume that this is implemented
  //  def reduceR[A,Z] (opr: (A,Z) => Z) (t: FingerTree[A], z: Z) :Z = ??? // assume that this is implemented

  //  trait FingerTree[+A] {
  //  def addL[B >:A] (b: B) :FingerTree[B] = ??? // assume that this is implemented as in the paper
  //  def addR[B >:A] (b: B) :FingerTree[B] = ??? // assume that this is implemented as in the paper
  // }

  // Implement this:

  def concatenate[A, B >: A] (left: FingerTree[A]) (right: FingerTree[B]) :FingerTree[B] = ???

} // Q7


object Q8 {

  // def nullOption[T] = Lens[...]

  // Answer the questions below:

  // A. ...

  // B. ...

  // C. ...

} // Q8

