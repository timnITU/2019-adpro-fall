// Advanced Programming, Exercises by A. Wąsowski, IT University of Copenhagen
//
// AUTHOR1:
// AUTHOR2:
//
// Write ITU email addresses of both group members that contributed to
// the solution of the exercise (in lexicographic order).
//
// You should work with the file by following the associated exercise sheet
// (available in PDF from the course website).
//
// The file is meant to be compiled inside sbt, using the 'compile' command.
// To run the compiled file use the 'run' or 'runMain' command.
// To load the file int the REPL use the 'console' command.
// Now you can interactively experiment with your code.
//
// Continue solving exercises in the order presented in the PDF file. The file
// shall always compile, run, and pass tests, after you are done with each
// exercise (if you do them in order).  Please compile and test frequently.

// The extension of App allows writing the main method statements at the top
// level (the so called default constructor). For App objects they will be
// executed as if they were placed in the main method in Java.
package fpinscala

object Exercises extends App with ExercisesInterface {

  import fpinscala.List._

  // Exercise 3

  // add @annotation.tailrec to make the compiler check that your solution is
  // tail recursive
  def fib (n: Int) : Int = 
  {
    @annotation.tailrec
    def recu (m: Int, last: Int, current: Int) : Int = 
    {
      if (m < 1) -1
      if (m == 1) last
      else recu(m-1, current, current+last)
    }
    recu(n,0,1)
  }

  // Exercise 4

  // add @annotation.tailrec to make the compiler check that your solution is
  // tail recursive
  def isSorted[A] (as: Array[A], ordered: (A,A) =>  Boolean) :Boolean = 
  {
    @annotation.tailrec
    def recu (n: Int) : Boolean = 
    {
      if (n >= as.length-1) true
      else if (!ordered(as(n),as(n+1))) false
      else recu(n+1)
    }
    recu(0)
  }

  // @annotation.tailrec
  // def isSorted2[A] (as: Array[A], ordered: (A,A) =>  Boolean) :Boolean = as match 
  // {
  //   case Nil => true
  //   case x :: Nil => true
  //   case x :: x2 :: xs if !ordered(x,x2) => false
  //   case x :: x2 :: xs => isSorted2(x2::xs, ordered)
  // }

  // Exercise 5

  def curry[A,B,C] (f: (A,B)=>C): A => (B => C) =
  {
    (a:A) => f(a,_)
  }

  // Exercise 6

  def uncurry[A,B,C] (f: A => B => C): (A,B) => C = 
  {
    ((a:A),(b:B)) => f(a)(b)
  }

  // Exercise 7

  def compose[A,B,C] (f: B => C, g: A => B) : A => C = 
  {
    (a:A) => f(g(a))
  }

  // Exercise 8 requires no programming

  //3

  // Exercise 9
  def tail[A] (as: List[A]) :List[A] = as match {
    case Cons(_,xs) => xs
    case Nil => throw new Exception("List is empty")
  }

  // Exercise 10

  @annotation.tailrec
  // Uncommment the annotation after solving to make the
  // compiler check whether you made the solution tail recursive
  def drop[A] (l: List[A], n: Int) : List[A] = l match{
    case _ if n == 0 => l
    case Cons(_,xs) if n > 0 => drop(xs, n-1)
    case Nil => throw new Exception("List is empty")
    case _ => throw new Exception("weird")
  }

  // Exercise 11

   @annotation.tailrec
  // Uncommment the annotation after solving to make the
  // compiler check whether you made the solution tail recursive
  def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match{
    case Cons(x,_) if !f(x) => l
    case Cons(x,xs) if f(x) => dropWhile(xs, f)
    case Nil => List[A] ()
    case _ => throw new Exception("weird")
  }

  // Exercise 12

  def init[A](l: List[A]): List[A] = l match {
    case Nil => throw new Exception("List is empty")
    case Cons(_, Nil) => List[A] ()
    case Cons(x, xs) => Cons(x, init(xs))
    case _ => throw new Exception("weird")
  }

  // Exercise 13

  def length[A] (as: List[A]): Int = foldRight(as,0)((_,y) => 1+y)

  // Exercise 14

  // @annotation.tailrec
  // Uncommment the annotation after solving to make the
  // compiler check whether you made the solution tail recursive
  def foldLeft[A,B] (as: List[A], z: B) (f: (B, A) => B): B = 
    as match {
      case Nil => z
      case Cons(x, xs) => foldLeft(xs, f(z,x))(f)
  }

  // Exercise 15

  def product (as: List[Int]): Int = foldLeft(as,1)((a,i) => a*i)

  def length1 (as: List[Int]): Int = foldLeft(as,0)((y,_) => 1+y)

  // Exercise 16

  def reverse[A] (as: List[A]): List[A] = foldLeft(as,List[A]())((x,y) => Cons(y,x))

  // Exercise 17

  def foldRight1[A,B] (as: List[A], z: B) (f: (A, B) => B): B = foldLeft(reverse(as),z)((a,i) => f(i,a))

  // Exercise 18

  def foldLeft1[A,B] (as: List[A], z: B) (f: (B,A) => B): B = foldRight(as, (b:B) => b)((a,g) => b => g(f(b,a)))(z)

  // Exercise 19

  def append[A](a1: List[A], a2: List[A]): List[A] = a1 match {
    case Nil => a2
    case Cons(h,t) => Cons(h, append(t, a2))
  }

  def concat[A] (as: List[List[A]]): List[A] = foldLeft(as,List[A]())(append)

  // Exercise 20

  def filter[A] (as: List[A]) (f: A => Boolean): List[A] = foldRight(as,List[A]()) ((a,y) => if (f(a)) Cons(a,y) else y)

  // Exercise 21

  def flatMap[A,B](as: List[A])(f: A => List[B]): List[B] = foldRight(as,List[B]()) ((a,y) =>append(f(a),y))

  // Exercise 22

  def filter1[A] (l: List[A]) (p: A => Boolean) :List[A] = flatMap(l) ((a) => if (p(a)) Cons(a,Nil) else Nil)

  // Exercise 23

  def add (l: List[Int]) (r: List[Int]): List[Int] = (l,r) match {
    case (Nil, _) => Nil
    case (_, Nil) => Nil
    case (Cons(l1,l2), Cons(r1,r2)) => Cons(l1+r1, add(l2)(r2))
  }

  // Exercise 24

  def zipWith[A,B,C] (f: (A,B)=>C) (l: List[A], r: List[B]): List[C] = (l,r) match {
    case (Nil, _) => Nil
    case (_, Nil) => Nil
    case (Cons(l1,l2), Cons(r1,r2)) => Cons(f(l1,r1), zipWith(f)(l2,r2))
  }
  // Exercise 25

  def checker[A](as: List[A], bs: List[A]) : Boolean = 
    (as,bs) match {
      case (_, Nil) => true
      case (Cons(a1,a2),Cons(b1,b2)) if a1 == b1 => checker(a2,b2)
      case _ => false
  }

  def hasSubsequence[A] (sup: List[A], sub: List[A]): Boolean = 
    sup match {
      case _ if sub == Nil => true
      case Nil => sub == Nil
      case _ if checker(sup, sub) => true 
      case Cons(_,xs) => hasSubsequence(xs, sub)
      case _ => false
    }

  // Exercise 26

  def pascal (n: Int): List[Int] =
  {
    def go (counter: Int) (prev: List[Int]): List[Int] = 
      prev match {
        case _ if counter == n => prev
        case _ if counter == 0 => go (counter + 1) (Cons(1,Nil))
        case _ if counter == 1 => go (counter + 1) (Cons(1,Cons(1, Nil)))
        case _ if counter == 2 => go (counter + 1) (Cons(1,Cons(2, Cons(1, Nil))))
        case Cons(x, Nil) => Cons(x,Nil)
        case Cons(x,Cons(x2,xs)) if x == 1 && x2 != 1 => go (counter+1) (Cons(1,Cons(x+x2,go (counter) (Cons(x2,xs)))))
        case Cons(x, Cons(x2,xs)) => Cons(x+x2,go (counter) (Cons(x2,xs)))
        
    }
    go(0) (List[Int]())
  }

}
