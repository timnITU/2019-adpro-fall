package adpro
import org.scalatest.FunSuite
import Stream._

class Stream_test extends FunSuite {
  
  test("This one always works: (-1) * (-1) = 1") {
    assert((-1)*(-1)==1);
  }
  
  //Sanity check of exercise 1
  test("Exercise 1: The first element of Stream.from(3) is 3") {
    assert(from(3).headOption().contains(3));
  }

  test("Exercise 1: The second element of Stream.from(3) is 4") {
    assert(from(3).tail.headOption().contains(4));
  }

	test("Exercise 1: The first element of Stream.to(3) is 3") {
    assert(to(3).headOption().contains(3));
  }
  
	test("Exercise 1: The second element of Stream.to(3) is 2") {
    assert(to(3).tail.headOption().contains(2));
  }

	test("Exercise 1: The first element of naturals is 0"){
		assert(naturals.headOption().contains(0))
	}


	test("Exercise 2: The Stream(1,2,3).toList is List(1,2,3) "){
		  val l2 :Stream[Int]= cons(1, cons(2, cons (3, empty)))
			assert(l2.toList(0) == 1)
			assert(l2.toList(1) == 2)
			assert(l2.toList(2) == 3)


	}

	test("Exercise 3: naturals.take(3)  is Stream(1,2,3) "){
		  val l2 :Stream[Int]= naturals.take(3) 
			assert(l2.toList(0) == 0)
			assert(l2.toList(1) == 1)
			assert(l2.toList(2) == 2)
  }

  test("Exercise 3: naturals.drop(3) is Stream(3,4,5,...) "){
		  val l2 :Stream[Int]= naturals.drop(3) 
			assert(l2.headOption().contains(3))
      assert(l2.tail.headOption().contains(4))
      assert(l2.tail.tail.headOption().contains(5))

			//assert(l2.toList(2) == 5)
  }

  test("Exercise 3: naturals.take(1000000000).drop(41).take(10).toList is Stream(41, 42, 43, 44, 45, 46, 47, 48, 49, 50) "){
		  val l2 :Stream[Int]= naturals.take(1000000000).drop(41).take(10)
			assert(l2.headOption().contains(41))
      assert(l2.tail.headOption().contains(42))
      assert(l2.tail.tail.headOption().contains(43))
      assert(l2.tail.tail.tail.headOption().contains(44))
      assert(l2.tail.tail.tail.tail.headOption().contains(45))
      assert(l2.tail.tail.tail.tail.tail.headOption().contains(46))
      assert(l2.tail.tail.tail.tail.tail.tail.headOption().contains(47))
      assert(l2.tail.tail.tail.tail.tail.tail.tail.headOption().contains(48))
      assert(l2.tail.tail.tail.tail.tail.tail.tail.tail.headOption().contains(49))
      assert(l2.tail.tail.tail.tail.tail.tail.tail.tail.tail.headOption().contains(50))
  }

  test("Exercise 4: naturals.takeWhile(_<1000000000).drop(100).take(50)"){
		  val l2 :Stream[Int]= naturals.takeWhile(_<1000000000).drop(100).take(50)
			assert(l2.headOption().contains(100))
  }


  test("Exercise 5: naturals.forAll (_ < 0) should be false"){
		  val l2 :Boolean= naturals.forAll(_ < 0)
			assert(!l2)
  }

  test ("Exercise 5: catching an exception") {
    val thrown = intercept[StackOverflowError] {
      naturals.forAll (_ >=0)
    }
    assert(thrown.getMessage === null)
  }

  test("Exercise 6: naturals.takeWhile2(_<1000000000).drop(100).take(50)"){
		  val l2 :Stream[Int]= naturals.takeWhile2(_<1000000000).drop(100).take(50)
			assert(l2.headOption().contains(100))
  }
  
  test("Exercise 4: naturals.takeWhile(_ % 3 < 2).take(10)"){
		  val l2 :Stream[Int] = naturals.takeWhile(_ % 3 < 2).take(10)
			assert(l2.headOption().contains(0))
      assert(l2.tail.headOption().contains(1))
  }

  test("Exercise 6: naturals.takeWhile2(_ % 3 < 2).take(10).toList"){
		  val l2 :Stream[Int] = naturals.takeWhile2(_ % 3 < 2).take(10)
			assert(l2.headOption().contains(0))
      assert(l2.tail.headOption().contains(1))
  }

//Sanity check of exercise 1
  test("Exercise 7: The first element of Stream.from(3) is 3, using headOption2") {
    assert(from(3).headOption2().contains(3));
  }

  test("Exercise 7: The second element of Stream.from(3) is 4, using headOption2") {
    assert(from(3).tail.headOption2().contains(4));
  }

	test("Exercise 7: The first element of Stream.to(3) is 3, using headOption2") {
    assert(to(3).headOption2().contains(3));
  }
  
	test("Exercise 7: The second element of Stream.to(3) is 2, using headOption2") {
    assert(to(3).tail.headOption2().contains(2));
  }

	test("Exercise 7: The first element of naturals is 0, using headOption2"){
		assert(naturals.headOption2().contains(0))
	}

  test("Exercise 8.1 - map: naturals.map (_*2).take (20)"){
		  val l2 :Stream[Int] = naturals.map(_*2).take(20)
			assert(l2.headOption().contains(0))    
  }


  test("Exercise 8.1 - map: naturals.map (_*2).drop (30).take (50).toList"){
		  val l2 :List[Int] = naturals.map (_*2).drop (30).take (50).toList
      val l3 :List[Int] = 60 to 158 by 2 toList 
      val buh:Boolean = l2 == l3 
			assert(buh)    
  }

  test("Exercise 8.2 - filter: naturals.filter(_ % 2 == 0).take(10)"){
		  val l2 :Stream[Int] = naturals.filter(_ % 2 == 0).take(10)
			assert(l2.headOption().contains(0))
      assert(l2.tail.headOption().contains(2))
      assert(l2.tail.tail.headOption().contains(4))
  }

  test("Exercise 8.2 - filter: naturals.drop(42).filter (_%2 ==0).take (30).toList"){
		  val l2 :List[Int] = naturals.drop(42).filter (_%2 ==0).take (30).toList
      val l3 :List[Int] = 42 to 100  by 2 toList
      val buh:Boolean = l2 == l3 
      assert(buh) 
	}

  test("Exercise 8.3 - append: naturals.take(10).append(naturals).take(20).toList"){
		  val l2 :Stream[Int] = naturals.append(naturals)
      assert(true) 
	}

  test("Exercise 8.3 - append: naturals.drop(42).filter (_%2 ==0).take (30).toList"){
		  val l2 :List[Int] = naturals.take(10).append(naturals).take(20).toList
      val l3 :List[Int] = (0 to 9 toList) ::: (0 to 9 toList)
      assert(l2 == l3) 
	}

  // test("Exercise 8.4 - flatMap: naturals.flatMap (to _).take (100).toList"){
	// 	  val l2 :List[Int] = naturals.flatMap(to _).take(100).toList
  //     val l3 :List[Int] = (0 to 9 toList) ::: (0 to 9 toList)
  //     assert(true) 
	// }

  test("Exercise 10 - fibs: fibs.take(7).toList == List(0,1,1,2,3,5,8)"){
      val l1: List[Int] = empty.fibs.take(7).toList
      val l2: List[Int] = List(0,1,1,2,3,5,8)
      val l4: Boolean = l1 == l2
      assert(l4) 
	}

//   test("Exercise 11 - unfold: from(1).take(1000000000).drop (41).take(10).toList == from2(1).take(1000000000).drop (41).take(10).toList"){
//      // val l1: List[Int] = empty.unfold(naturals)(x => x.headOption()).take(50).toList
//       val l2: List[Int] = naturals.take(50).toList 
//       val l4: Boolean = l2 == l2
//       assert(l4) 
// 	}


//   test("Exercise 12.1 - from: from(1).take(1000000000).drop (41).take(10).toList == from2(1).take(1000000000).drop (41).take(10).toList"){
//       val l1: List[Int] = from(1).take(1000000000).drop (41).take(10).toList
//       val l2: List[Int] = empty.from2(1).take(1000000000).drop (41).take(10).toList 
//       val l4: Boolean = l1 == l2
//       assert(l4) 
// 	}

//   test("Exercise 12.2 - fibs: fib2.take(100).toList == fibs.take(100).toList"){
//       val l1: List[Int] = empty.fib2.take(100).toList
//       val l2: List[Int] = empty.fibs.take(100).toList 
//       val l4: Boolean = l1 == l2
//       assert(l4) 
// 	}

//     test("Exercise 13.1 - map2: naturals.map2 (_*2).take (20)"){
// 		  val l2 :Stream[Int] = naturals.map2(_*2).take(20)
// 			assert(l2.headOption().contains(0))    
//   }


//   test("Exercise 13.1 - map2: naturals.map2 (_*2).drop (30).take (50).toList"){
// 		  val l2 :List[Int] = naturals.map2 (_*2).drop (30).take (50).toList
//       val l3 :List[Int] = 60 to 158 by 2 toList 
//       val buh:Boolean = l2 == l3 
// 			assert(buh)    
//   }
// 	test("Exercise 13.2 - take2: naturals.take2(3)  is Stream(1,2,3) "){
// 		  val l2 :Stream[Int]= naturals.take2(3) 
// 			assert(l2.toList(0) == 0)
// 			assert(l2.toList(1) == 1)
// 			assert(l2.toList(2) == 2)
//   }

//   test("Exercise 13.2 - take2: naturals.take2(1000000000).drop(41).take2(10).toList is Stream(41, 42, 43, 44, 45, 46, 47, 48, 49, 50) "){
// 		  val l2 :Stream[Int]= naturals.take2(1000000000).drop(41).take2(10)
// 			assert(l2.headOption().contains(41))
//       assert(l2.tail.headOption().contains(42))
//       assert(l2.tail.tail.headOption().contains(43))
//       assert(l2.tail.tail.tail.headOption().contains(44))
//       assert(l2.tail.tail.tail.tail.headOption().contains(45))
//       assert(l2.tail.tail.tail.tail.tail.headOption().contains(46))
//       assert(l2.tail.tail.tail.tail.tail.tail.headOption().contains(47))
//       assert(l2.tail.tail.tail.tail.tail.tail.tail.headOption().contains(48))
//       assert(l2.tail.tail.tail.tail.tail.tail.tail.tail.headOption().contains(49))
//       assert(l2.tail.tail.tail.tail.tail.tail.tail.tail.tail.headOption().contains(50))
//   }

//   test("Exercise 13.3 - takewhile3: naturals.takeWhile3(_<1000000000).drop(100).take(50)"){
// 		  val l2 :Stream[Int]= naturals.takeWhile3(_<1000000000).drop(100).take(50)
// 			assert(l2.headOption().contains(100))
//   }

//   test("Exercise 13.4 - zipWith: naturals.zipWith[Int,Int] (_+_) (naturals).take(2000000000).take(20).toList"){
// 		  val l2 :List[Int]= naturals.zipWith2[Int,Int] (_+_) (naturals).take(2000000000).take(20).toList
// 			val l3 :List[Int] = (0 to 38 by 2 toList)
//       val l4 :Boolean = l2 == l3
//       assert(l4)
//   }
/*
  test("Quiz - n = 12, 13"){
		  val l2 :List[Int]= even_from.toList
			val l3 :List[Int] = (0 to 38 by 2 toList)
      val l4 :Boolean = l2 == l3
      assert(l4)
  }*/

}