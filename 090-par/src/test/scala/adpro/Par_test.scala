package adpro
import org.scalatest.FunSuite
import Par._
import java.util.concurrent.Executors

class Par_test extends FunSuite {
  
  test("This one always works: (-1) * (-1) = 1") {
    assert((-1)*(-1)==1);
  }
  
  //write  your tests below.

  test ("asyncF") {
    val ex = Executors.newFixedThreadPool(16)
    def f : Int => Par[Int] = Par.asyncF((x:Int) => x*2)
    val d = Par.run(ex)(f(3)).get
	  assert(d == 6)
  }

  test("Map") {
    val list = List(1,2,3,4)
    val a = Par.run(Executors.newFixedThreadPool(16))(Par.parMap(list)((x:Int) => x*2)).get
    assert(List(2,4,6,8) == a)
  }
  
  test("Filter") {
    val list = List(1,2,3,4)
    val a = Par.run(Executors.newFixedThreadPool(16))(Par.parFilter(list)((x:Int) => x % 2 == 0)).get
    assert(List(2,4) == a)
  }

  test("Map3") {
    val a = Par.unit(3)
	  val b = Par.unit(4)
	  val c = Par.unit(5)
    val d = Par.run(Executors.newFixedThreadPool(16))(Par.map3(a,b,c)((x,y,z) => x * y * z)).get
    assert(60 == d)
  }
  
  test("ChoiceN") {
    val ex = Executors.newFixedThreadPool(16)
	  val i = Par.unit(2)
	  val list = List(2,4,6,8)
	  val listPar = list.foldRight(List():List[Par[Int]])((x:Int,y:List[Par[Int]]) => Par.unit(x)::y)
    val d = Par.run(ex)(Par.choiceN(i)(listPar)).get
    assert(6 == d)
  }
  
  test("Choice") {
    val ex = Executors.newFixedThreadPool(16)
	  val i = Par.unit(false)
	  val a = Par.unit(3)
	  val b = Par.unit(6)
    val d = Par.run(ex)(Par.choice(i)(a,b)).get
    assert(3 == d)
  }

  
  // test("ChoiceNviaChooser") {
  //   val ex = Executors.newFixedThreadPool(16)
	//   val i = Par.unit(2)
	//   val list = List(2,4,6,8)
	//   val listPar = list.foldRight(List():List[Par[Int]])((x:Int,y:List[Par[Int]]) => Par.unit(x)::y)
  //   val d = Par.run(ex)(Par.choiceNviaChooser(i)(listPar)).get
  //   assert(6 == d)
  // }
  
  // test("ChoiceViaChooser") {
  //   val ex = Executors.newFixedThreadPool(16)
	//   val i = Par.unit(false)
	//   val a = Par.unit(3)
	//   val b = Par.unit(6)
  //   val d = Par.run(ex)(Par.choiceViaChooser(i)(a,b)).get
  //   assert(6 == d)
  // }
  
  // test("Join") {
  //   val ex = Executors.newFixedThreadPool(16)
  //   val i = Par.unit(Par.unit(3))
  //   val j = Par.run(ex)(i).get
	//   val k = Par.run(ex)(j).get
  //   val d = Par.run(ex)(Par.join(i)).get
	// assert(k == d)
  // }
  
  // test("JoinViaFlatmap") {
  //   val ex = Executors.newFixedThreadPool(16)
  //   val i = Par.unit(Par.unit(3))
  //   val j = Par.run(ex)(i).get
	//   val k = Par.run(ex)(j).get
  //   val d = Par.run(ex)(Par.joinViaFlatmap(i)).get
	// assert(k == d)
  // }


}