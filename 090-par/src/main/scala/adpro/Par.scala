package adpro
import java.util.concurrent._
import scala.language.implicitConversions

// Work through the file top-down, following the exercises from the week's
// sheet.  Uncomment and complete code fragments.

object Par {

  type Par[A] = ExecutorService => Future[A]
  def run[A] (s: ExecutorService) (a: Par[A]) : Future[A] = a(s)


  case class UnitFuture[A] (get: A) extends Future[A] {
    def isDone = true
    def get (timeout: Long, units: TimeUnit) = get
    def isCancelled = false
    def cancel (evenIfRunning: Boolean) : Boolean = false
  }

  def unit[A] (a: A) :Par[A] = (es: ExecutorService) => UnitFuture(a)

  def map2[A,B,C] (a: Par[A], b: Par[B]) (f: (A,B) => C) : Par[C] =
    (es: ExecutorService) => {
      val af = a (es)
      val bf = b (es)
      UnitFuture (f(af.get, bf.get))
    }

  def fork[A] (a: => Par[A]) : Par[A] = es => es.submit(
    new Callable[A] { def call = a(es).get }
  )

  def lazyUnit[A] (a: =>A) : Par[A] = fork(unit(a))

  // Exercise 1 (CB7.4)

  def asyncF[A,B] (f: A => B) : A => Par[B] = 
    (a:A) => lazyUnit(f(a))

  // map is shown in the book

  def map[A,B] (pa: Par[A]) (f: A => B) : Par[B] =
    map2 (pa,unit (())) ((a,_) => f(a))

  // Exercise 2 (CB7.5)

  def sequence[A] (ps: List[Par[A]]): Par[List[A]] = 
    ps match {
      case Nil => unit(Nil)
      case (x :: xs) => map2(x, sequence(xs)) (_::_)
    }

  def sequence20[A] (ps: List[Par[A]]): Par[List[A]] = 
    es => UnitFuture(ps.map(p => p(es).get))

  // Exercise 3 (CB7.6)

  // this is shown in the book:

  def parMap[A,B](ps: List[A])(f: A => B): Par[List[B]] = fork {
    val fbs: List[Par[B]] = ps.map(asyncF(f))
    sequence(fbs)
  }

  def parFilter[A](as: List[A])(f: A => Boolean): Par[List[A]] = fork {
    val fbs: List[Par[List[A]]] = as.map(asyncF((x:A) => if(f(x)) List(x) else Nil ))
    map(sequence(fbs))(_.flatten)
  }

  // Exercise 4: implement map3 using map2

  def map3[A,B,C,D] (pa :Par[A], pb: Par[B], pc: Par[C]) (f: (A,B,C) => D) :Par[D]  = 
  {
    map2 (pa, map2(pb,pc)((b,c) => (b,c))) ((a,bc) => f(a,bc._1,bc._2))
  }
//Paw!!!! -original intent was similar, but had problems exposing pb and pc, needed the identity function

  // shown in the book

  def equal[A](e: ExecutorService)(p: Par[A], p2: Par[A]): Boolean = p(e).get == p2(e).get

  // Exercise 5 (CB7.11)

   def choiceN[A] (n: Par[Int]) (choices: List[Par[A]]) :Par[A] = 
    es => {
      //We run the "choices"  and this the parellel computation contained in choices
      //with the value obtained by running the parallel computation of n.
      run(es)(choices(run(es)(n).get)) 
    }

  def choice[A] (cond: Par[Boolean]) (t: Par[A], f: Par[A]) : Par[A] =
  {
    //Starts by mapping the boolean into a int value, and then constructs the list
    //from the two parallel computations t and f.
    choiceN(map(cond)((x:Boolean) => if (x) 1 else 0))(List(t, f))
  }
  // Exercise 6 (CB7.13)

//The idea is the same as for choice? Just with different types?
  //Atleast I can't see another way of implementing it?
  def chooser[A,B] (pa: Par[A]) (choices: A => Par[B]): Par[B] =
    es => {
      //We run the "choices"  and this the parellel computation contained in choices
      //with the value(of type A) obtained by running the parallel computation of pa.
      run(es)(choices(run(es)(pa).get)) 
  }

  def choiceNviaChooser[A] (n: Par[Int]) (choices: List[Par[A]]) :Par[A] =
  {
    chooser(n)(int => choices(int))
  }

  def choiceViaChooser[A] (cond: Par[Boolean]) (t: Par[A], f: Par[A]) : Par[A] =
  {
    chooser(cond)(bool => if (bool) t else f)
  }

  // Exercise 7 (CB7.14)

  // def join[A] (a : Par[Par[A]]) :Par[A] =

  class ParOps[A](p: Par[A]) {

  }

  implicit def toParOps[A](p: Par[A]): ParOps[A] = new ParOps(p)
}
